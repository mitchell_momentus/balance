//
//  Constants.swift
//  Balance
//
//  Created by Mitchell on 25/06/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import UIKit

class Constants {
    class var worldCategory:UInt32 {
        return 1 << 1;
    }
    
    class var ballCategory:UInt32 {
        return 1 << 2;
    }
    
    class var holeCategory:UInt32 {
        return 1 << 3;
    }
    
    class var ballHoleCategory:UInt32 {
        return 1 << 4;
    }
    
    class var ballSize:CGSize {
        return CGSizeMake( 30, 30)
    }
    
    class var holeSize:CGSize {
        return CGSizeMake( 30, 30)
    }
    
    class var holeColor:UIColor{
        return UIColor.blackColor()
    }
    
    class var holeFillColor:UIColor{
        return UIColor.grayColor()
    }
    
}
