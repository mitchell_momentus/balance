//
//  Ball.swift
//  Balance
//
//  Created by Mitchell on 11/06/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import SpriteKit
import Foundation

class Ball : SKSpriteNode{
    
    func setup(#x: Int, y: Int){
        self.physicsBody = SKPhysicsBody(circleOfRadius: Constants.ballSize.width/2);
        self.position = CGPoint(x: Int(x),y: Int(y))
        self.size = Constants.ballSize;
        self.physicsBody.restitution = 0.5;
        self.physicsBody.affectedByGravity = false;
        self.physicsBody.mass = 0.01
        self.physicsBody.allowsRotation = false
        self.physicsBody.charge = 100
        self.zPosition = 2
        self.physicsBody.collisionBitMask = Constants.ballCategory | Constants.worldCategory
        self.physicsBody.contactTestBitMask = Constants.holeCategory
        self.physicsBody.categoryBitMask = Constants.ballCategory
    }
    class func disableBarrier () -> UInt32{
        return Constants.ballCategory | Constants.worldCategory
    }
    
    class func enableBarrier () -> UInt32{
        return Constants.ballCategory | Constants.worldCategory | Constants.holeCategory
    }
}
