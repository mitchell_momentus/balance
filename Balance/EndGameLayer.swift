//
//  EndGameLayer.swift
//  Balance
//
//  Created by Mitchell on 1/07/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import UIKit
import SpriteKit

protocol EndGameDelegate{
    func restartGame()
}


class EndGameLayer: GameHelperLayer {
    var spriteNode:SKSpriteNode?
    var delegate:EndGameDelegate?
    var playButton:SKSpriteNode?
    var gameOverNode:SKSpriteNode?
    var timerNode:SKLabelNode!
    var bestTimeNode:SKLabelNode!
    var bestTimeAlertNode:SKLabelNode!
    init(frame: CGRect) {
        super.init(frame: frame)
        
        gameOverNode = SKSpriteNode(imageNamed: "GameOverText")
        gameOverNode!.position = CGPointMake(frame.width * 0.5, frame.height * 0.80)
        self.addChild(gameOverNode)
        
        playButton = SKSpriteNode(imageNamed: "PlayButton")
        playButton!.position = CGPointMake(frame.width * 0.5, frame.height * 0.30)
        self.addChild(playButton)
        
        timerNode = SKLabelNode(text: "Completed In: 0:00")
        timerNode.position = CGPointMake(frame.width * 0.5, frame.height * 0.50)
        timerNode.fontSize = 42
        timerNode.fontColor = UIColor.blackColor()
        timerNode.fontName = "04b_19"
        self.addChild(timerNode)
        
        bestTimeNode = SKLabelNode(text: "Best Time: 0:00")
        bestTimeNode.position = CGPointMake(frame.width * 0.5, frame.height * 0.45)
        bestTimeNode.fontSize = 42
        bestTimeNode.fontColor = UIColor.blackColor()
        bestTimeNode.fontName = "04b_19"
        self.addChild(bestTimeNode)
        
        bestTimeAlertNode = SKLabelNode(text: "Best Time: 0:00")
        bestTimeAlertNode.position = CGPointMake(frame.width * 0.5, frame.height * 0.65)
        bestTimeAlertNode.fontSize = 42
        bestTimeAlertNode.fontColor = UIColor.redColor()
        bestTimeAlertNode.fontName = "04b_19"
        bestTimeAlertNode.hidden = true
        bestTimeAlertNode.text = "New Best Time!"
        self.addChild(bestTimeAlertNode)
        
        println("Point at \(self.frame.width) + \(self.frame.height)")
        
    }
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        var touch:UITouch = touches.anyObject() as UITouch
        var location = touch.locationInNode(self)
        
        if(self.playButton!.containsPoint(location)){
            delegate?.restartGame()
        }
        
    }
 
    
    func endGame (time:String, bestTime:String, bestTimeAlert:Bool){
//        createTimerNode(time)
        bestTimeNode.text = bestTime
        timerNode!.text = time
        if bestTimeAlert {
            bestTimeAlertNode.hidden = false
        }
    }
}
