//
//  hole.swift
//  Balance
//
//  Created by Mitchell on 11/06/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit
import CoreMotion

class Hole : SKShapeNode{
    var joint:SKPhysicsJointLimit! = nil
    func setup(#x: Int, y: Int){
        self.position = CGPoint(x: x,y:y)
        var radius = (Constants.holeSize.width/2) - (Constants.ballSize.width/4)
        self.physicsBody = SKPhysicsBody(circleOfRadius: abs(radius))
        self.physicsBody.dynamic = false
        self.physicsBody.collisionBitMask = Constants.worldCategory
        self.physicsBody.categoryBitMask = Constants.holeCategory
        self.strokeColor = Constants.holeColor
        self.zPosition = 1
        self.fillColor = Constants.holeFillColor
    }
    func disable (){
        self.physicsBody.categoryBitMask = Constants.worldCategory
    }
    
    func enable (){
        self.physicsBody.categoryBitMask = Constants.ballHoleCategory
    }
    
}