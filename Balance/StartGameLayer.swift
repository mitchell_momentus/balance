//
//  StartGameLayer.swift
//  Balance
//
//  Created by Mitchell on 1/07/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import UIKit
import SpriteKit

protocol StartGameDelegate{
    func startGame()
}


class StartGameLayer: GameHelperLayer {
    var spriteNode:SKSpriteNode?
    var delegate:StartGameDelegate?
    var playButton:SKSpriteNode?
    init(frame: CGRect) {
        super.init(frame: frame)
        playButton = SKSpriteNode(imageNamed: "PlayButton")
        playButton!.position = CGPointMake(frame.width * 0.5, frame.height * 0.30)
        self.addChild(playButton!)
        spriteNode = playButton!
    }
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        var touch:UITouch = touches.anyObject() as UITouch
        var location = touch.locationInNode(self)
        
        if(self.playButton?.containsPoint(location)){
            delegate?.startGame()
        }
        
    }
}
