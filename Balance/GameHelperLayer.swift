//
//  GameHelperLayer.swift
//  Balance
//
//  Created by Mitchell on 1/07/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import UIKit
import SpriteKit
class GameHelperLayer: SKNode {

    init(frame: CGRect) {
            super.init()
            var color = UIColor(white: 1, alpha: 0)
            var node = SKSpriteNode(color: color, size: frame.size)
        node.anchorPoint = CGPointMake(CGRectGetMidX(frame), CGRectGetMidY(frame));
            addChild(node);
            node.zPosition = -1;
            node.name = "transparent";
    }

}
