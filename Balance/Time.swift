//
//  Time.swift
//  Balance
//
//  Created by Mitchell on 1/07/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import UIKit

class Time: NSObject {
    var mins:Int = 0
    var secs:Int = 0
    
    init() {
        mins = 0;
        secs = 0;
    }
    
    init(secs:Int, mins:Int){
        self.mins = mins
        self.secs = secs
    }
    
    init(newTime:Time){
        self.mins = newTime.mins
        self.secs = newTime.secs
    }
    
    func reset(){
        mins = 0;
        secs = 0;
    }
    
    func addSecond(){
        if(secs==59){
            secs = 0;
            mins++;
        }else{
            secs++;
        }
    }
    
    func get()->(minutes:Int,seconds:Int){
        return(mins,secs)
    }
    
    func eqz() -> Bool { //Equals Zero
        if mins == 0 && secs == 0 {
            return true
        }
        return false
    }
}