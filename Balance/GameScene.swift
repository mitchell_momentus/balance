//
//  GameScene.swift
//  Balance
//
//  Created by Mitchell on 11/06/14.
//  Copyright (c) 2014 Mitchell. All rights reserved.
//

import SpriteKit
import UIKit
import CoreMotion

class GameScene: SKScene , SKPhysicsContactDelegate , StartGameDelegate, EndGameDelegate{
    let NUM_BALLS = 5;
    var balls = Ball[]()
    var holes = Hole[]()
    var joints = SKPhysicsJointLimit[]()
    var bgTexture:SKSpriteNode!
    var motionManager:CMMotionManager = CMMotionManager();
    let holeStickyness:Float = 10;
    var enabled = false
    var jointCount = 0
    var highScore:Time = Time()
    var timerNode:SKLabelNode!
    var timer:NSTimer?
    var time:Time = Time()
    
    var startGameLayer:StartGameLayer?
    var endGameLayer:EndGameLayer?
    
    init() {
        var mins = NSUserDefaults().integerForKey("Mins")
        var secs = NSUserDefaults().integerForKey("Secs")
        highScore = Time(secs: secs, mins: mins)
        if(highScore.eqz()){
            highScore = Time(secs: Int.max, mins: Int.max)
        }
        super.init()
    }
    init(coder aDecoder: NSCoder!) {
        var mins = NSUserDefaults().integerForKey("Mins")
        var secs = NSUserDefaults().integerForKey("Secs")
        highScore = Time(secs: secs, mins: mins)
        if(highScore.eqz()){
            highScore = Time(secs: Int.max, mins: Int.max)
        }
        println("Best Time = \(highScore.mins):\(highScore.secs)")
        super.init(coder: aDecoder)
        //        createTimerNode()
        initializeStartGameLayer()
        showGameStartLayer()
        
    }
    
    func updateTimer() {
        // Something cool
        time.addSecond()
        if timerNode != nil {
            timerNode!.text = formatTime(time);
        }
    }
    func formatTime ( time:Time ) -> String {
        return "\(time.mins):"+NSString(format:"%.2d", time.secs)
    }
    
    func initializeEndGameLayer(){
        endGameLayer = EndGameLayer(frame: self.frame)
        endGameLayer!.userInteractionEnabled = true;
        endGameLayer!.zPosition = 150;
        endGameLayer!.delegate = self;
    }
    
    func showEndGameLayer(){
        self.addChild(endGameLayer)
    }
    
    func initializeStartGameLayer(){
        startGameLayer = StartGameLayer(frame: self.frame)
        startGameLayer!.userInteractionEnabled = true;
        startGameLayer!.zPosition = 150;
        startGameLayer!.delegate = self;
    }
    
    func showGameStartLayer(){
        self.removeBalls()
        self.addChild(startGameLayer)
    }
    
    override func didMoveToView(view: SKView) {
        addBackground()
        self.timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        self.physicsBody.categoryBitMask = Constants.worldCategory
        self.physicsBody.collisionBitMask = Constants.ballCategory
        self.physicsWorld.contactDelegate = self
        var queue:NSOperationQueue = NSOperationQueue()
        motionManager.accelerometerUpdateInterval = (1/40)
        motionManager.startAccelerometerUpdatesToQueue(queue) { accelerometerData, error in
            
            var dxIn:CGFloat = CGFloat(accelerometerData.acceleration.y) * 100
            var dyIn:CGFloat = CGFloat(-accelerometerData.acceleration.x) * 100
            accelerometerData.acceleration
            var force = CGVectorMake(dxIn, dyIn)
            let forceVector:CGFloat = abs(force.dx+force.dy)
            
            if self.enabled {
                for ball in self.balls {
                    ball.physicsBody.applyForce(force)
                }
                for hole in self.holes {
                    if(hole.joint != nil){
                        if forceVector > CGFloat(self.holeStickyness) {
                            self.physicsWorld.removeJoint(hole.joint)
                            hole.joint = nil
                            self.jointCount--
                        }
                    }
                }
            }
            
            
            
        }
        
    }
    var stopTimeDelegate:StopTime?
    func registerDelegate( stop:StopTime ){
        stopTimeDelegate = stop
    }
    
    func removeHoles(){
        holes.removeAll(keepCapacity: false)
    }
    
    func addBackground(){
        bgTexture = SKSpriteNode(imageNamed:"bg")
        bgTexture.position = CGPoint(x: self.size.width/2,y: self.size.height/2)
        addChild(bgTexture)
    }
    
    func addHoles(){
        
        var millis = NSDate().timeIntervalSince1970*1000
        srand(millis.bridgeToObjectiveC().unsignedIntValue)
        
        var frameX:Int = Int(frame.width)
        var frameY:Int = Int(frame.height)
        var minX = Int(Float(frameX) * 0.10)
        var minY = Int(Float(frameY) * 0.10)
        var maxX:Int = frameX - minX
        var maxY:Int = frameY - minY
        var midX:Int = (frameX) / 2
        var midY:Int = (frameY) / 2
        
        for i in 1...NUM_BALLS {
            let hole:Hole = Hole(circleOfRadius: Constants.holeSize.width/2)
            var notIntersects:Bool = true
            var xpos:Int!
            var ypos:Int!
            
            while notIntersects {
                var rndX:Int = Int(rand())
                var rndY:Int = Int(rand())
                
                xpos = (rndX % (maxX-minX)) + minX
                ypos = (rndY % (maxY-minY)) + minY
                
                if holes.count > 0 {
                    for tmpHole in holes {
                        if tmpHole.frame.contains(CGPoint(x: xpos!, y: ypos!)){
                            notIntersects = true
                            println("here")
                            break
                        }else{
                            notIntersects = false
                        }
                    }
                } else {
                    notIntersects = false
                }
            }
            
            hole.setup(x:xpos!, y: ypos!)
            holes.append(hole)
            addChild(hole)
        }
    }
    
    func removeBalls(){
        balls.removeAll(keepCapacity: false);
    }
    
    func addBalls(){
        var millis = NSDate.timeIntervalSinceReferenceDate()
        srand(millis.bridgeToObjectiveC().unsignedIntValue)
        for i in 1...NUM_BALLS {
            var ball:Ball = Ball(imageNamed:"Metal_Sphere")
            var notIntersects:Bool = true
            var xpos:Int?
            var ypos:Int?
            while notIntersects {
                xpos = Int(rand()) % Int(self.frame.width)
                ypos = Int(rand()) % Int(self.frame.height)
                if balls.count > 0 {
                    for ball in balls {
                        if ball.frame.contains(CGPoint(x: xpos!, y: ypos!)){
                            notIntersects = true
                            break
                        }else{
                            notIntersects = false
                        }
                    }
                } else {
                    notIntersects = false
                }
            }
            
            println("Added ball at x = \(xpos!) y = \(ypos!)")
            ball.setup(x:xpos!, y:ypos!)
            balls.append(ball)
            addChild(ball);
        }
    }
    
    
    
    func didBeginContact(contact: SKPhysicsContact) {
        var ball:Ball
        var hole:Hole
        if contact.bodyA.categoryBitMask == Constants.ballCategory{
            ball = contact.bodyA.node as Ball
            hole = contact.bodyB.node as Hole
        } else {
            ball = contact.bodyB.node as Ball
            hole = contact.bodyA.node as Hole
        }
        var holeMid = CGPoint(x:CGRectGetMidX(hole.frame), y: CGRectGetMidY(hole.frame))
        var ballMid = CGPoint(x:CGRectGetMidX(ball.frame), y: CGRectGetMidY(ball.frame))
        if(hole.joint==nil){ //Only one joint per hole.
            hole.joint = SKPhysicsJointLimit.jointWithBodyA(hole.physicsBody, bodyB: ball.physicsBody, anchorA: holeMid, anchorB: ballMid)
            hole.joint.maxLength = Constants.ballSize.width/4
            self.physicsWorld.addJoint(hole.joint)
            jointCount++
        }
        if jointCount == NUM_BALLS { //if we have gotten all of the balls
            endGame()
        }
    }
    
    func createTimerNode(){
        timerNode = SKLabelNode(fontNamed:"04b_19")
        timerNode.text = "0:00"
        timerNode.fontSize = 42;
        timerNode.fontColor = UIColor.blackColor()
        timerNode.position = CGPointMake(timerNode.frame.width/2,0);
        self.addChild(timerNode)
    }
    
    func startGame() {
        enabled = true
        self.startGameLayer!.removeFromParent()
        self.addBackground()
        self.addHoles()
        self.addBalls()
        time = Time()
        createTimerNode()
    }
    
    func endGame() {
        timerNode.removeFromParent()
        self.removeHoles()
        self.removeBalls()
        jointCount = 0;
        initializeEndGameLayer()
        showEndGameLayer()
        var scoreText:String = String()
        var newBestTime = false
        println("Current Time = \(time.mins):\(time.secs)")
        println("Best Time = \(highScore.mins):\(highScore.secs)")
        if time.mins <= highScore.mins && time.secs < highScore.secs {
            NSUserDefaults().setInteger(time.mins, forKey: "Mins")
            NSUserDefaults().setInteger(time.secs, forKey: "Secs")
            highScore = time
            newBestTime = true
        }
        scoreText += "Time " + formatTime(time)
        var bestTimeText = "Best Time " + formatTime(highScore)
        endGameLayer?.endGame(scoreText,bestTime:bestTimeText,bestTimeAlert: newBestTime)
    }
    func restartGame() {
        self.removeAllChildren()
        endGameLayer?.removeFromParent()
        startGame()
    }
}

protocol StopTime{
    func stopTime()
}
//